class Config:
    TEST_IMAGE_PATH = "data/new_train_image"
    TEST_LABLE_PATH =  "data/new_train_label"
    TEST_IMAGE_FILE = 'data/train.txt'
    TRAIN_IMAGE_PATH = "data/new_train_image"
    TRAIN_LABEL_PATH = "data/new_train_label"
    TRAIN_IMAGE_FILE = 'data/train.txt'
    USE_C = 1
    RESTORE = 0
    LEARNING_RATE = 0.0001
    DECAY = 0.9
    ITERS_PER_DECAY = 1000
    TOTAL_EPOCHS = 160
    BATCHSIZE = 4
    TOTAL_SAMPLES = 7167


